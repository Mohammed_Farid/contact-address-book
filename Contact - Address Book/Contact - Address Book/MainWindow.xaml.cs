﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Contact___Address_Book
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public class Contact
        {
            public string Name        { get; set; } // to be binded
            public int    PhoneNumber { get; set; } // to be binded
            public string Address     { get; set; } // to be binded
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            // Holds the phone number
            int phone;

            // Checks if none of the textboxes is empty and the phone number is a real number
            if( nameTxt.Text != string.Empty && phoneTxt.Text != string.Empty && addressTxt.Text != string.Empty && int.TryParse(phoneTxt.Text, out phone))
            {
                // Creates new object
                Contact newContact = new Contact()
                {
                    Name = nameTxt.Text,
                    PhoneNumber = phone,
                    Address = addressTxt.Text
                };

                ConactWindow.Items.Add(newContact);
                noteArea.Text = string.Empty;
            }
            // if the name textbox is empty, change the note
            else if(nameTxt.Text == string.Empty)
            {
                noteArea.Text = "No name was entered.";
            }
            // if the phone textbox is empty, change the note
            else if (phoneTxt.Text == string.Empty)
            {
                noteArea.Text = "No number was entered.";
            }
            // if the name address is empty, change the note
            else if (addressTxt.Text == string.Empty)
            {
                noteArea.Text = "No address was entered.";
            }
            // if the phone text isn't a number, change the note
            else if (!int.TryParse(phoneTxt.Text, out phone))
            {
                noteArea.Text = "Wrong numbers format.";
            }
        }

        private void removeBtn_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = ConactWindow.SelectedItem;
            if (selectedItem != null)
            {
                ConactWindow.Items.Remove(selectedItem);
            }
        }
    }
}
